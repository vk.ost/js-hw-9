"use strict"

function showArray(arr, domElement = document.body) {
    const fragment = document.createDocumentFragment();
    arr.forEach((element)=>{
        const innerLi = document.createElement("li");
        if(Array.isArray(element) === true){
            showArray(element, innerLi);
        }else{
            innerLi.textContent = element;
        }
        fragment.append(innerLi);
     });
    const list = document.createElement("ul");
    list.classList.add("list")
    list.append(fragment);
    domElement.append(list);
}


function clearPage() {
    const timerText = document.createElement("span");
    timerText.classList.add("timer-text")
    timerText.textContent="До удaления: ";
    document.body.append(timerText);
    const timerCount = document.createElement("span");
    timerCount.classList.add("timer-count")
    let count = 3;
    timerCount.textContent=count;
    document.body.append(timerCount);
    const lineWrap = document.createElement("div");
    lineWrap.classList.add("line-wrapper");
    document.body.append(lineWrap);
    const line = document.createElement("div");
    line.classList.add("line");
    lineWrap.append(line);
    let timerId = setInterval(() => {
        --count;
        timerCount.textContent=count;
        line.befor(timerCount);
    }, 1000);
    setTimeout(()=>{
        clearTimeout(timerId);
        removeChildren(document.body);
    },3000)
}
function removeChildren(element) {
    for (let i = element.children.length-1; i >=0; i--) {
        element.children[i].remove();
    }
}

showArray(["Kharkiv",["Borispol", "Irpin", ["Borispol", "Irpin"]], "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]);
clearPage();